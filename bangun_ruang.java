
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class bangun_ruang {
	
	    public static void showMenu() {
	        System.out.println("Menu Rumus / formula");
	        System.out.println("1. Persegi / square");
	        System.out.println("2. Persegi Panjang / rectangle");
	        System.out.println("3. Segitiga Siku - Siku / right triangle");
	        System.out.println("4. Belah Ketupat / rhomb ");
	        System.out.println("5. Layang - Layang / kite");
	        System.out.println("6. Jajar Genjang /palalelogram");
	        System.out.println("7. Trapesium / trapezoid ");
	        System.out.println("8. Lingkaran / circle");
	        System.out.println("9. Tabung / cylinder ");
	        System.out.println("10.Kubus / cube");
	        System.out.println("11.Balok / beam ");
	        System.out.println("12.Limas / pyramid ");
	        System.out.println("13.Kerucut / conical");
	        System.out.println("14.Prisma / prism");
	        System.out.println("15.Bola / sphere");
	        System.out.println("0. Keluar / out");
	    }

	    public static void squareFormula() {
	        System.out.println("Masukan Panjang Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputData = null;
	        try {
	            inputData = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        try  {
	            float side = Float.parseFloat(inputData);
	            float wide = side * side;
	            System.out.println("Luas Persegi: " + wide);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Inputan Tidak Sesuai");
	        }
	    }

	    public static void rectangleFormula() {
	        System.out.println("Masukan Panjang Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataLength = null;
	        try {
	            inputDataLength = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        System.out.println("Masukan Lebar Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataWidth = null;
	        try {
	            inputDataWidth = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        try  {
	            float length = Float.parseFloat(inputDataLength);
	            float width = Float.parseFloat(inputDataWidth);
	            float wide = length * width;
	            System.out.println("Luas Persegi Panjang: " + wide);
	            
	            float around = 2*(length + width);
	            System.out.println("Keliling Persegi Panjang: " + around);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Inputan Tidak Sesuai");
	        }
	    }

	    public static void triangleFormula() {
	        System.out.println("Masukan Panjang Sisi Segitiga (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataLength = null;
	        try {
	            inputDataLength = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        System.out.println("Masukan Lebar Sisi Segitiga (Format Angka Bilangan Desimal ex: 1.0): ");
	        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataWidth = null;
	        try {
	            inputDataWidth = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }

	        try  {
	            float length = Float.parseFloat(inputDataLength);
	            float width = Float.parseFloat(inputDataWidth);
	            double wide = 0.5 * length * width;
	            System.out.println("Luas Segitiga : " + wide);
	            float around = 2*(length + width);
	            System.out.println("Keliling segitiga : " + around);          
	            
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Inputan Tidak Sesuai");
	        }
	    }

	    public static void rhombFormula(){
	    	System.out.println("Masukkan diagonal 1 (Format Angka Bilangan Desimal ex: 1.0): ");
	    	BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    	String inputDataDiagonal1 = null;
	    	try {
	    		inputDataDiagonal1 = bufferedReader.readLine();
	    	}
	    	catch (IOException error){
	    		System.out.println("Error Input "+error.getMessage());
	    	}
	    	System.out.println("Masukkan diagonal 2 (Format Angka Bilangan Desimal ex: 1.0): ");
	    	 BufferedReader BufferedReader = new BufferedReader (new InputStreamReader(System.in));
			    String inputDataDiagonal2 = null;
			    try{
			    	inputDataDiagonal2 = bufferedReader.readLine();
			    }
			    catch (IOException error){
			    	System.out.println("Error Input "+error.getMessage());	
			    }
			    System.out.println("Masukkan Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
		    	BufferedReader buferedReader = new BufferedReader (new InputStreamReader(System.in));
		    	String inputDataSide = null;
		    	try{
		    		inputDataSide = bufferedReader.readLine();
		    	}
		    	catch (IOException error){
		    		System.out.println("Error Input "+error.getMessage());
		    		try {	
			    		 float diagonal1 = Float.parseFloat(inputDataDiagonal1);
			    		 float diagonal2 = Float.parseFloat(inputDataDiagonal2);
				         float side = Float.parseFloat(inputDataSide);         
				         double wide = 0.5 * diagonal1 * diagonal2;
				         System.out.println("Luas Belah Ketupat : " + wide);
				         float around = 4*side;
				         System.out.println("Keliling Belah Ketupat : " + around);   
			    	
			    	 }
			        catch(NumberFormatException e) {
			            System.out.println("Inputan Tidak Sesuai");
			    	}	
	    }
	    }
	    public static void kiteFormula(){
	    	System.out.println("Masukkan diagonal 1 (Format Angka Bilangan Desimal ex: 1.0): ");
	    	BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputDataDiagonal1 = null;
	    	try{
	    		inputDataDiagonal1 = bufferedReader.readLine();
	    	}
	    	catch (IOException error){
	    		System.out.println("Error Input "+error.getMessage());
	    		
	    	}
	    	System.out.println("Masukkan diagonal 2 (Format Angka Bilangan Desimal ex: 1.0): ");
		    BufferedReader BufferedReader = new BufferedReader (new InputStreamReader(System.in));
		    String inputDataDiagonal2 = null;
		    try{
		    	inputDataDiagonal1 = bufferedReader.readLine();
		    }
		    catch (IOException error){
		    	System.out.println("Error Input "+error.getMessage());	
		    	
		    }
		    System.out.println("Masukkan Sisi 1 (Format Angka Bilangan Desimal ex: 1.0): ");
	    	BufferedReader buferedReader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputDataSide1 = null;
	    	try{
	    		inputDataSide1 = bufferedReader.readLine();
	    	}
	    	catch (IOException error){
	    		System.out.println("Error Input "+error.getMessage());
	    	}
	    	System.out.println("Masukkan Sisi 2 (Format Angka Bilangan Desimal ex: 1.0): ");
	    	BufferedReader buferedreader = new BufferedReader (new InputStreamReader(System.in));
	    	String inputDataSide2 = null;
	    	try{
	    		inputDataDiagonal1 = bufferedReader.readLine();
	    	}
	    	catch (IOException error){
	    		System.out.println("Error Input "+error.getMessage());
	    		
	    	try {	
	    		 float diagonal1 = Float.parseFloat(inputDataDiagonal1);
	    		 float diagonal2 = Float.parseFloat(inputDataDiagonal2);
		         float side1 = Float.parseFloat(inputDataSide1);
		         float side2 = Float.parseFloat(inputDataSide2);
		         double wide = 0.5 * diagonal1 * diagonal2;
		         System.out.println("Luas Layang-Layang : " + wide);
		         float around = 2*(side1 + side2);
		         System.out.println("Keliling Layang-Layang : " + around);   
	    	
	    	 }
	        catch(NumberFormatException e) {
	            System.out.println("Inputan Tidak Sesuai");
	    	}
	    }
	    	
	    }  
	    public static void palalelogramFormula(){
	    	 System.out.println("Masukan Alas (Format Angka Bilangan Desimal ex: 1.0): ");
		        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		        String inputDataBase = null;
		        try {
		            inputDataBase = bufferedReader.readLine();
		        }
		        catch (IOException error) {
		            System.out.println("Error Input " + error.getMessage());
		        }
		        System.out.println("Masukan Tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
		        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		        String inputDatahight = null;
		        try {
		            inputDatahight = bufferedReader.readLine();
		        }
		        catch (IOException error) {
		            System.out.println("Error Input " + error.getMessage());
		        }
		        System.out.println("Masukan Sisi Miring (Format Angka Bilangan Desimal ex: 1.0): ");
		        BufferedReader buffEredreader = new BufferedReader(new InputStreamReader(System.in));
		        String inputDataHypotenusa = null;
		        try {
		            inputDataHypotenusa = bufferedReader.readLine();
		        }
		        catch (IOException error) {
		            System.out.println("Error Input " + error.getMessage());

		        try  {
		            float Base = Float.parseFloat(inputDataBase);
		            float hight = Float.parseFloat(inputDatahight);
		            float Hypotenusa = Float.parseFloat(inputDataHypotenusa);
		            float wide = Base * hight;
		            System.out.println("Luas Jajar Genjang: " + wide);
		            float around = 2*(Hypotenusa + hight);
		            System.out.println("Keliling Jajar Genjang: " + around);
		        }
		        catch(NumberFormatException e) {
		            System.out.println("Inputan Tidak Sesuai");
		    }
		    }
	    }
	    
	    public static void trapezoidFormula(){
	    	System.out.println("Masukan Sisi a (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataSideA = null;
	        try {
	            inputDataSideA = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Sisi b (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataSideB = null;
	        try {
	            inputDataSideB = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Sisi c (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader buferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataSideC = null;
	        try {
	            inputDataSideC = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Sisi d (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader Bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataSideD = null;
	        try {
	            inputDataSideD = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Sisi Sejajar A (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader Buferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataParalelsidesA = null;
	        try {
	            inputDataParalelsidesA = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Sisi Sejajar C (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader BuferedreAder = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataParalelsidesC = null;
	        try {
	            inputDataParalelsidesC = bufferedReader.readLine();
	        }
	    
	        catch (IOException error) {
	           System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader BufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataHight = null;
	        try {
	            inputDataHight = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        try  {
	            float SideA = Float.parseFloat(inputDataSideA);
	            float SideB = Float.parseFloat(inputDataSideB);
	            float SideC = Float.parseFloat(inputDataSideC);
	            float SideD = Float.parseFloat(inputDataSideD);
	            float Hight = Float.parseFloat(inputDataHight);
	            float ParalelsidesA = Float.parseFloat(inputDataParalelsidesA);
	            float ParalelsidesC = Float.parseFloat(inputDataParalelsidesC);
	            double wide = 0.5 *(ParalelsidesA + ParalelsidesC) *Hight;
	            System.out.println("Luas Trapesium: " + wide);
	            float around = SideA + SideB + SideC + SideC;
	            System.out.println("Keliling Trapesium: " + around);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Inputan Tidak Sesuai");
	    }
	    }
	    public static void circleFormula(){
	        System.out.println("Masukan Phi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader Bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataPhi = null;
	        try {
	            inputDataPhi = Bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Diameter (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReAder = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataDiameter = null;
	        try {
	            inputDataDiameter = Bufferedreader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	           
	            float Phi = Float.parseFloat(inputDataPhi);
	            float Diameter = Float.parseFloat(inputDataDiameter);
	            double Radius = (0.5*Diameter);
	            System.out.println("Jari-Jari : "+Radius);
	            double wide = Phi*(2*Radius);
	            System.out.println("Luas Lingkaran : " + wide);
	            double around = 2*(Phi*Radius);
	            System.out.println("Keliling Lingkaran: " + around);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Inputan Tidak Sesuai");
	    }
	    }
	    public static void cylinderFormula(){
	    	System.out.println("Masukan Phi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataPhi = null;
	        try {
	            inputDataPhi = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Jari - Jari (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader BufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataRadius = null;
	        try {
	            inputDataRadius = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataHight = null;
	        try {
	            inputDataHight = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        try  {
		           
	            float Phi = Float.parseFloat(inputDataPhi);
	            float Radius = Float.parseFloat(inputDataRadius);
	            float Hight = Float.parseFloat(inputDataHight);
	            float wide = 2*Phi*Radius*(Hight+Radius);
	            System.out.println("Luas Permukaan Tabung : "+ wide);
	            double volume = Phi*(Math.pow(Radius, 2))*Hight;
	            System.out.println("Volume Tabung : "+ volume);
	        }
	        catch(NumberFormatException e) {
		        	System.out.println("Inputan Tidak Sesuai");
        }
	    }
	      
	    
	    public static void cubeFormula(){
	    	 System.out.println("Masukan Panjang Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
		        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		        String inputDataSide = null;
		        try {
		            inputDataSide = bufferedReader.readLine();
		        }
		        catch (IOException error) {
		            System.out.println("Error Input " + error.getMessage());
		        }

		        try  {
		            float side = Float.parseFloat(inputDataSide);
		            double wide = 6* (Math.pow(side, 2));
		            System.out.println("Luas Kubus : " + wide);
		            double volume = (Math.pow(side, 3));
		            System.out.println("Volume Kubus : "+ volume);
		        
		        }
		        catch(NumberFormatException e) {
		        	System.out.println("Inputan Tidak Sesuai");
        }
	    }
	    
	    

	        
	        
	    
	    public static void beamFormula(){
	    	System.out.println("Masukan Panjang (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataLength = null;
	        try {
	            inputDataLength = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bUfferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataHight = null;
	        try {
	            inputDataHight = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Lebar (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReAder = new BufferedReader(new InputStreamReader(System.in));
	        String inputDatawidth = null;
	        try {
	            inputDatawidth = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float Length = Float.parseFloat(inputDataLength);
	            float Hight = Float.parseFloat(inputDataHight);
	            float width = Float.parseFloat(inputDatawidth);
	            float wide = 2*(Length*width + Length*Hight + width*Hight);
	            System.out.println("Luas Kubus : " + wide);
	            float volume = (Length*width*Hight);
	            System.out.println("Volume Balok : "+ volume);
	        
	        }
	        catch(NumberFormatException e) {
	        	System.out.println("Inputan Tidak Sesuai");
    }
    }
    
	    
	    public static void pyramidFormula(){
	    	System.out.println("Masukan Alas (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataBase = null;
	        try {
	            inputDataBase = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Sisi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader buffeRedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataSide = null;
	        try {
	            inputDataSide = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Panjang (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader BufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataLength = null;
	        try {
	            inputDataLength = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Lebar (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bUfferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataWidth = null;
	        try {
	            inputDataWidth = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReAder = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataHight = null;
	        try {
	            inputDataHight = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        try  {
	            float Base = Float.parseFloat(inputDataBase);
	            float Side = Float.parseFloat(inputDataSide);
	            float Length = Float.parseFloat(inputDataLength);
	            float Width = Float.parseFloat(inputDataWidth);
	            float Hight = Float.parseFloat(inputDataHight);
	            float wide = Base*4*Side;
	            System.out.println("Luas Limas : " + wide);
	            double volume = 0.3*Length*Width*Hight;
	            System.out.println("Volume Limas : "+ volume);
	        }
	        catch(NumberFormatException e) {
	        	System.out.println("Inputan Tidak Sesuai");
    }
	        
	    }
	    public static void conicalFormula(){
	    	System.out.println("Masukan Phi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataPhi = null;
	        try {
	            inputDataPhi = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataHigth = null;
	        try {
	            inputDataHigth = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Alas (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReAder = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataBase = null;
	        try {
	            inputDataBase = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Jari - Jari (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader Bufferedreader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataRadius = null;
	        try {
	            inputDataRadius = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        try  {
	            float Phi = Float.parseFloat(inputDataPhi);
	            float Higth = Float.parseFloat(inputDataHigth);
	            float Base = Float.parseFloat(inputDataBase);
	            float Radius = Float.parseFloat(inputDataRadius);
	            float wide = Phi*Radius*(Radius + Base) ;
	            System.out.println("Luas Kerucut : " + wide);
	            double volume = 0.3*Phi*(Math.pow(Radius, 2* Higth));
	            System.out.println("Volume Kerucut : "+ volume);
	        }
	        catch(NumberFormatException e) {
	        	System.out.println("Inputan Tidak Sesuai");
	    }
	    }
	    public static void prismFormula(){
	    	System.out.println("Masukan Alas (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataBase = null;
	        try {
	            inputDataBase = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Tinggi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader buFferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataHigth = null;
	        try {
	            inputDataHigth = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        System.out.println("Masukan Keliling (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader BufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataAround = null;
	        try {
	            inputDataAround = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        try  {
	            float Base = Float.parseFloat(inputDataBase);
	            float Higth = Float.parseFloat(inputDataHigth);
	            float Around = Float.parseFloat(inputDataAround);
	            float wide = 2*Base + Around*Higth;
	            System.out.println("Luas Prism : " + wide);
	            double volume = Base*Higth;
	            System.out.println("Volume Prism : "+ volume);
	        
	    }
        catch(NumberFormatException e) {
        	System.out.println("Inputan Tidak Sesuai");
        }
	    }
	    public static void sphereFormula(){
	    	System.out.println("Masukan Phi (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataPhi = null;
	        try {
	            inputDataPhi = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        System.out.println("Masukan Jari - Jari (Format Angka Bilangan Desimal ex: 1.0): ");
	        BufferedReader bUfferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataRadius = null;
	        try {
	            inputDataRadius = bufferedReader.readLine();
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	    }
	        try  {
	            float Phi = Float.parseFloat(inputDataPhi);
	            float Radius = Float.parseFloat(inputDataRadius);
	            double Wide = 4*Phi*(Math.pow(Radius, 2));
	            System.out.println("Luas Lingkaran : " + Wide);   
	            double volume = 1.3*Phi*(Math.pow(Radius, 3));
	            System.out.println("Volume Lingkaran : "+ volume);
	        
	        }
	        catch(NumberFormatException e) {
	        	System.out.println("Inputan Tidak Sesuai");
    }
	    }
   

	    
		public static void main(String[] args) {
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputData = null;
	        int choice = 0;
	        do {
	            showMenu();
	            System.out.println("Masukkan Pilihan Anda: ");
	            try {
	                inputData = bufferedReader.readLine();
	                try  {
	                    choice = Integer.parseInt(inputData);
	                    if (choice > 0 && choice == 1) {
	                        squareFormula();
	                    }
	                    else if (choice > 0 && choice == 2) {
	                        rectangleFormula();
	                    }
	                    else if (choice > 0 && choice == 3) {
	                        triangleFormula();
	                    }
	                    else if (choice > 0 && choice == 4) {
	                        rhombFormula();
	                    }
	                    else if (choice > 0 && choice == 5) {
	                        kiteFormula();
	                    }
	                    else if (choice > 0 && choice == 6) {
	                        palalelogramFormula();
	                    }
	                    else if (choice > 0 && choice == 7) {
	                        trapezoidFormula();
	                    }
	                    else if (choice > 0 && choice == 8) {
	                        circleFormula();
	                    }
	                    else if (choice > 0 && choice == 9) {
	                        cylinderFormula();
	                    }
	                    else if (choice > 0 && choice == 10) {
	                        cubeFormula();
	                    }
	                    else if (choice > 0 && choice == 11) {
	                        beamFormula();
	                    }
	                    else if (choice > 0 && choice == 12) {
	                        pyramidFormula();
	                    }
	                    else if (choice > 0 && choice == 13) {
	                        conicalFormula();
	                    }
	                    else if (choice > 0 && choice == 14) {
	                        prismFormula();
	                    }
	                    else if (choice > 0 && choice == 15) {
	                        sphereFormula();
	                    }
	                    else {
	                        System.out.println(".........Thank You........");
	                    }
	                }
	                catch(NumberFormatException e) {
	                    System.out.println("Inputan Tidak Sesuai");
	                }
	            }
	            catch (IOException error) {
	                System.out.println("Error Input " + error.getMessage());
	            }

	        } while(choice > 0);
	    }
	

}
